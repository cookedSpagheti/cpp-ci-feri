#include <vector>
#include <string>
#include <fstream>
#include <chrono>
#include <iostream>

void merge(std::vector< std::vector<unsigned long> >* polje, unsigned long low, unsigned long high, unsigned long middle);
void mergeSort(std::vector< std::vector<unsigned long> >* polje, unsigned long low, unsigned long high);


int main(int argc, char **argv) {
	std::vector< std::vector<unsigned long> > read_numbers;
	read_numbers.reserve(100000);

	std::ifstream file;
	file.open(argv[1]);
	unsigned long max_range = 0;

	if (file.is_open()) {
		std::string line;
		size_t pos = 0;

		std::getline(file, line);   /// tole se uporab
		pos = line.find(" ");
		max_range = std::stoi(line.substr(0, pos));

		while (!file.eof()) {
			try {
				std::vector<unsigned long> vec;
				vec.reserve(2);

				std::getline(file, line);
				pos = line.find(" ");

				vec.push_back(std::stoi(line.substr(0, pos)));
				vec.push_back(std::stoi(line.substr(pos)));
				read_numbers.push_back(vec);
			}
			catch(const std::exception &ex) {
				break;
			}
		}

		file.close();
	}
	else return 1;


	std::vector< std::vector<unsigned long> > stations_length;

	stations_length.reserve(read_numbers.size());

	for (unsigned long i = 0; i < read_numbers.size(); ++i) {
		std::vector<unsigned long> vec;
		vec.reserve(2);

		vec.push_back(read_numbers[i][0] - read_numbers[i][1]);
		vec.push_back(read_numbers[i][0] + read_numbers[i][1]);
		stations_length.push_back(vec);
	}


	auto tic = std::chrono::steady_clock::now();
	mergeSort(&stations_length, 0, stations_length.size()-1);


	unsigned long index = 0,
		increment_index = 0;
	long removed_num = 0;

	std::vector< std::vector<unsigned long> > saved_stations;
	saved_stations.reserve(stations_length.size());


	while (stations_length.size() > index) {
		unsigned long current_begining = stations_length[index][0];

		// ce se trenutni pas ne za�ne po/pred koncem zadnjega shranjenega 
		if (saved_stations.size() > 0) {

			// preveri ali je za�etek trenutnega enak zadnjemu shranjenemu pasu
			if (saved_stations.back()[0] == current_begining) {

				// in �e je konec trenutnega manj�i od konca zadnjega shranjenega, se trenutnega spusti (primer E)
				if (saved_stations.back()[1] > stations_length[index][1]) {
					++removed_num;
					++index;
					continue;
				}
			}

			// ce je za�etek trenutnega pas znotraj shranjenega pasu
			else if (saved_stations.back()[0] < current_begining && saved_stations.back()[1] > current_begining) {

				// preveri ali je konec trenutnega pasu manjsi od konca zadnjega shranjenega pasu -> trenutni pas se nahaja znotraj
				if (saved_stations.back()[1] > stations_length[index][1]) {
					++index;
					++removed_num;
					continue;
				}

				// preveri ali je konec trenutnega pasu enak koncu zadnjega shranjenega pasu -> trenutni pas je nahaja znotraj (primer D)
				else if (saved_stations.back()[1] == stations_length[index][1]) {
					++index;
					++removed_num;
					continue;
				}
			}

			if (saved_stations.size() > 1)
				if (saved_stations[saved_stations.size() - 2][1] < stations_length[index][0]) {
					// se ga doda
					//std::cout << "F\n";
				}
				else if (saved_stations[saved_stations.size() - 2][1] > stations_length[index][0]) {
					saved_stations.pop_back();
					++removed_num;
				}
				else if (saved_stations.back()[0] < current_begining && saved_stations.back()[1] < stations_length[index][1])
					if (saved_stations[saved_stations.size() - 2][1] >= current_begining) {
						saved_stations.pop_back();
						++removed_num;
					}
		}
		saved_stations.push_back(stations_length[index + increment_index]);
		index += increment_index + 1;
		increment_index = 0;
	}


	// preveri ali je kak�ni vmesni prostor prazni
	unsigned long max_index = saved_stations.size();

	if (removed_num != -1) {
		unsigned long startingRange = saved_stations[0][0],
			endingRange = saved_stations[0][1];

		for (unsigned long i = 1; i < saved_stations.size(); ++i) {
			if (saved_stations[i][0] <= endingRange) {
				endingRange = saved_stations[i][1];
			}
			else {
				removed_num = -1;
				break;
			}
		}

		if (removed_num != -1) {
			if (0 < startingRange || endingRange < max_range)
				removed_num = -1;
		}
	}

	auto toc = std::chrono::steady_clock::now() - tic;
	auto time_spent = (float)std::chrono::duration_cast<std::chrono::milliseconds>(toc).count() / 1000;

	std::fstream output;
    output.open(argv[2]);

    // Write to the file
	output << "Prebran " << argv[1] << "..." << std::endl;
    output << "Izhod: " << removed_num << std::endl;
    output << "Cas porabljen: " << time_spent << "s" << std::endl;

    // File Close
    output.close();
	return 0;
}



void merge(std::vector< std::vector<unsigned long> >* polje, unsigned long low, unsigned long high, unsigned long middle) {
	std::vector< std::vector<unsigned long> > temp_vec;
	temp_vec.reserve(polje->size());

	unsigned long i = low,
		j = middle + 1;


	while (i <= middle && j <= high)
	{
		if (polje->at(i)[0] < polje->at(j)[0]) {
			//temp_vec[k] = polje->at(i);
			//++k;
			temp_vec.push_back(polje->at(i));
			++i;
		}
		else {
			temp_vec.push_back(polje->at(j));
			++j;
		}
	}

	while (i <= middle) {
		temp_vec.push_back(polje->at(i));
		++i;
	}

	while (j <= high) {
		temp_vec.push_back(polje->at(j));
		++j;
	}

	for (i = 0; i < temp_vec.size(); ++i)
		polje->at(i + low) = temp_vec[i];
}

void mergeSort(std::vector< std::vector<unsigned long> >* polje, unsigned long low, unsigned long high) {
	if (low < high) {
		unsigned long mid = (low + high) / 2;
		mergeSort(polje, low, mid);
		mergeSort(polje, mid + 1, high);
		merge(polje, low, high, mid);
	}
}
